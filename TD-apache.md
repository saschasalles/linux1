# TD apache

## Création d'une VM

vous utiliserez le vagrant file suivant :

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.box_check_update = false
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "512"
  end
  config.vm.define "websrv" do |web|
    web.vm.hostname = "websrv"
    web.vm.network "private_network", ip: "192.168.56.98"
    web.vm.provision "init", type: "shell", run: "once",
      inline: <<-EOF
        yum -y install httpd
      EOF
  end
end
```

puis hop :

``` 
$ vagrant up
$ vagrant ssh websrv
[vagrant@websrv ~]$ sudo su -
[root@websrv ~]#
```

Vous obtenez une VM centos7 avec apache installé

## Observations

### Le package httpd

Les fichiers de configuration

```bash
[root@websrv ~]# rpm -ql httpd | grep /etc
/etc/httpd
/etc/httpd/conf
/etc/httpd/conf.d
/etc/httpd/conf.d/README
/etc/httpd/conf.d/autoindex.conf
/etc/httpd/conf.d/userdir.conf
/etc/httpd/conf.d/welcome.conf
/etc/httpd/conf.modules.d
/etc/httpd/conf.modules.d/00-base.conf
/etc/httpd/conf.modules.d/00-dav.conf
/etc/httpd/conf.modules.d/00-lua.conf
/etc/httpd/conf.modules.d/00-mpm.conf
/etc/httpd/conf.modules.d/00-proxy.conf
/etc/httpd/conf.modules.d/00-systemd.conf
/etc/httpd/conf.modules.d/01-cgi.conf
/etc/httpd/conf/httpd.conf
/etc/httpd/conf/magic
/etc/httpd/logs
/etc/httpd/modules
/etc/httpd/run
/etc/logrotate.d/httpd
/etc/sysconfig/htcacheclean
/etc/sysconfig/httpd
```

Les fichiers exécutables

```bash
[root@websrv ~]# rpm -ql httpd | grep bin/
/usr/sbin/apachectl
/usr/sbin/fcgistarter
/usr/sbin/htcacheclean
/usr/sbin/httpd
/usr/sbin/rotatelogs
/usr/sbin/suexec
```

### Activation du serveur web

démarrage :

```bash
[root@websrv ~]# systemctl start httpd.service
```

démarrage automatique :

```bash
[root@websrv ~]# systemctl enable httpd.service
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
```

## Prise en main

### test initial

lancez une connexion depuis votre navigateur sur l'ip host only de votre serveur :
	Plus simplement suivez cet URL
http://192.168.56.98 

Déposez un fichier index.html dans /var/www/html contenant :

```html
<!DOCTYPE html><html><body>
<h1>Titre 1</h1>
<p>Paragraph de texte </p>
</body>
</html>
```

Rafraîchissez la page.

Consultez les logs :

```bash
[root@websrv ~]# journalctl --no-pager -xeq -o short-iso _SYSTEMD_UNIT=httpd.service
2018-04-11T21:44:40+0200 websrv httpd[1459]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using fe80::a00:27ff:fe09:db6b. Set the 'ServerName' directive globally to suppress this message
```

Apache n'as pas de nom de serveur web!

Corrigez la config afin que ce message n'apparaisse plus au démarrage

Redémarrez apache :

```bash
[root@websrv ~]# systemctl restart httpd.service
```

Moi, je n'ai plus de nouveau message d'erreur :

```bash
[root@websrv ~]# journalctl --no-pager -xeq -o short-iso _SYSTEMD_UNIT=httpd.service
2018-04-11T21:44:40+0200 websrv httpd[1459]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using fe80::a00:27ff:fe09:db6b. Set the 'ServerName' directive globally to suppress this message
```

### Configuration

#### Directive listen

Le service écoute bien sur le port 80
```bash
[root@websrv ~]# ss -anpt | grep httpd
LISTEN     0      128         :::80                      :::*                   users:(("httpd",pid=3535,fd=4),("httpd",pid=3534,fd=4),("httpd",pid=3533,fd=4),("httpd",pid=3532,fd=4),("httpd",pid=3531,fd=4),("httpd",pid=3530,fd=4))
```

les directives Listen définissent sur quels ports TCP le processus se met à l'écoute.
```bash
[root@websrv ~]# grep Listen /etc/httpd/conf/httpd.conf | grep -v "^#"
Listen 80
```

#### Le DocumentRoot

```bash
[root@websrv ~]# grep "cumentRoo" /etc/httpd/conf/httpd.conf | grep -v "#"
DocumentRoot "/var/www/html"
```

C'est la racine du site internet, ce que l'on ajoute dans l'url après le serveur est le chemin d'accès relatif depuis le DocumentRoot du site

#### Les sections directory

Avec la commande suivante on ne garde que les lignes qui contiennent Directory ou " Option" (avec un espace devant Option) et on ne garde pas (grep -v) les ligne qui commence (^) par un #

```bash
[root@websrv ~]# grep -e "Directory" -e "Options" -e "Allow" /etc/httpd/conf/httpd.conf | grep -v "^ *#"
<Directory />
    AllowOverride none
</Directory>
<Directory "/var/www">
    AllowOverride None
</Directory>
<Directory "/var/www/html">
    Options Indexes FollowSymLinks
    AllowOverride None
</Directory>
    DirectoryIndex index.html
<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
</Directory>
```

La directive Directory définie une sous-arborescence pour Apache ; en définissant les options on défini ce qu'apache peu faire dans cette sous arborescence ; Aussi AllowOverride definis ce qu’il est possible de redéfinir ailleur (dans les fichiers .htaccess)

#### Les options

__Faisons un test :__

Ajoutez un liens symbolique (racourci) de `/var/www/html/slash` vers `/`

```bash
[root@websrv ~]# ln -s /  /var/www/html/slash
```

puis allez là : http://192.168.56.98/slash/ , que constatez vous ? 
> Ceci met en évidence qu'une mauvaise config apache est assez dangereuse.

__Explication :__

- L'option `FollowSymLinks` permet à apache de suivre les liens symbolique.
- L'option `Indexes` permet à apache de lister le contenu d'un dossier requêté si celui-ci ne contiens pas d'index 
- Apache ne traite pas la route "/slash/" comme "/" mais comme /var/www/html/slash/ et donc utilise l'option Indexes

Si on remplace cette option par : SymLinksIfOwnerMatch :

```bash
[root@websrv ~]# cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.orig
[root@websrv ~]# vi /etc/httpd/conf/httpd.conf
[root@websrv ~]# diff /etc/httpd/conf/httpd.conf.orig /etc/httpd/conf/httpd.conf
95c95
< #ServerName www.example.com:80
---
> ServerName websrv:80
144c144
<     Options Indexes FollowSymLinks
---
>     Options Indexes SymLinksIfOwnerMatch
```

> En utilisant des racourcis vi (se prononce vi ail) on peu faire ces modification de la façon suivante :
[esc]:144 (pour aller à la ligne 144)
ww (pour aller au 3ieme mot)
D (pour supprimer jusqu'a la fin de la ligne)
A (Pour ajoute à la fin de la ligne)
Enfin tappez le texte : SymlinksIfOwnerMatch
[esc]:x

On redémarre pour voir ?

```bash
[root@websrv ~]# systemctl restart httpd.service
```

La faille est-elle supprimée ?

Si maintenant, on supprime le lien symbolique et nous le créons de nouveau , mais cette fois-ci avec le compte apache (on donne les droits de créer des fichier sur le dossier avant).

```bash
[root@websrv ~]# rm /var/www/html/slash
[root@websrv ~]# chown apache.apache /var/www/html
[root@websrv ~]# sudo -u apache ln -s / /var/www/html/slash
```

Vérification :

```bash
[root@websrv ~]# ls -al /var/www/html
total 12
drwxr-xr-x. 2 apache apache 4096 Feb  1 23:21 .
drwxr-xr-x. 6 root   root   4096 Jan 31 21:22 ..
-rw-r--r--. 1 root   root     88 Feb  1 23:10 index.html
lrwxrwxrwx. 1 apache apache    1 Feb  1 23:21 slash -> /
```

Rafraîchissez la page.

Résultat: nous avons modifié la configuration afin que les liens symboliques ne soient suivi que si le propriétaire du liens est aussi le propriétaire de la destination.

Ainsi un dévelopeur mal intentionné pourra toujours livrer dans son package logiciel un liens vers / (discrètement) cela ne lui donnera pas la visibilité sur toute l'arborescence. Cependant cela est couteux en ressources.

Une autre solution plus pérène est de désactiver l'option indexes et de preciser dans les balises directory la directive `DirectoryIndex` index.html

Pour les autres options : http://httpd.apache.org/docs/2.4/fr/mod/core.html#options

- Indexes : permettre de browser l'arborescence
- ExecCGI : permettre apache d'exécuter du code (dangereux)
- none : aucune

Il faut __absolument__ définir les Options sinon on les accepte toutes (sauf multiviews).

### Les VirtualHosts

#### virtual host par socket

Chaque site écoute sur un socket TCP différent

Créons une configuration pour un site

```bash
[root@websrv ~]# vi /etc/httpd/conf.d/site1.conf
[root@websrv ~]# cat /etc/httpd/conf.d/site1.conf
# on écoute sur le port 81
Listen 81
<VirtualHost 192.168.56.98:81>
DocumentRoot /var/www/html/site1
ServerName site1
</VirtualHost>
```

Créons une autre configuration pour un autre site sur un autre port TCP :

```bash
[root@websrv ~]# sed 's/site1/site2/;s/81/82/' /etc/httpd/conf.d/site1.conf > /etc/httpd/conf.d/site2.conf
[root@websrv ~]# cat /etc/httpd/conf.d/site2.conf
# on écoute sur le port 82
Listen 82
<VirtualHost 192.168.56.98:82>
DocumentRoot /var/www/html/site2
ServerName site2
</VirtualHost>
```

On créer les répertoires document root :

```bash
[root@websrv ~]# mkdir /var/www/html/site1 /var/www/html/site2
```

et des fichiers index html pour chacun
```bash
[root@websrv conf.d]# sed 's/Titre 1/Site 1/' /var/www/html/index.html > /var/www/html/site1/index.html
[root@websrv conf.d]# sed 's/Titre 1/Site 2/' /var/www/html/index.html > /var/www/html/site2/index.html
[root@websrv conf.d]# cat /var/www/html/site?/index.html
<!DOCTYPE html><html><body>
<h1>Site 1</h1>
<p>Paragraph de texte </p>
</body>
</html>
<!DOCTYPE html><html><body>
<h1>Site 2</h1>
<p>Paragraph de texte </p>
</body>
</html>
```

On désactive se linux puis on redémarre :

```bash
[root@websrv ~]# setenforce 0
[root@websrv ~]# systemctl restart httpd.service
```

Puis on teste les sites web ainsi créé :
http://192.168.56.98:81/
http://192.168.56.98:82/


#### VirtualHost par IP

Chaque site écoute sur une ip dédiée

```bash
[root@websrv ~]# grep -v Listen /etc/httpd/conf.d/site1.conf |sed 's/56.98/56.97/ ; s/81/80/ ; s/site1/site3/' >/etc/httpd/conf.d/site3.conf
[root@websrv ~]# cat /etc/httpd/conf.d/site3.conf
# on écoute sur le port 80
<VirtualHost 192.168.56.97:80>
DocumentRoot /var/www/html/site3
ServerName site3
</VirtualHost>
```

Créons un troisieme document root un fichier index.html

```bash
[root@websrv ~]# mkdir /var/www/html/site3
[root@websrv ~]# sed 's/Titre 1/Site 3/' /var/www/html/index.html > /var/www/html/site3/index.html
```

Ajoutons l'addresse ip ponctuelement sur l'interface:

```bash
[root@websrv ~]# ip addr add 192.168.56.97/24 dev eth1
[root@websrv ~]# ip addr show dev eth1
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:5c:49:03 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.98/24 brd 192.168.56.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet 192.168.56.97/24 scope global secondary eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe5c:4903/64 scope link 
       valid_lft forever preferred_lft forever
```

On redémarre :

```bash
[root@websrv ~]# systemctl restart httpd.service
```

Testons le 3ieme site web :: http://192.168.56.97/

#### Les NameVirtualhost

Les virtual hosts par noms de site web

on ré-écrit le fichier de conf site3.conf en y plaçant 3 virtual hosts

```bash
[root@websrv ~]# echo NameVirtualHost 192.168.56.98:80 > /etc/httpd/conf.d/site3.conf
[root@websrv ~]# grep -v Listen /etc/httpd/conf.d/site1.conf | grep -v "#" |sed 's/81/80/;s/site1/site3/' >>/etc/httpd/conf.d/site3.conf
[root@websrv ~]# grep -v Listen /etc/httpd/conf.d/site1.conf | grep -v "#" |sed 's/81/80/;s/site1/site4/' >>/etc/httpd/conf.d/site3.conf
[root@websrv ~]# grep -v Listen /etc/httpd/conf.d/site1.conf | grep -v "#" |sed 's/81/80/;s/site1/site5/' >>/etc/httpd/conf.d/site3.conf
[root@websrv ~]# cat /etc/httpd/conf.d/site3.conf
NameVirtualHost 192.168.56.98:80

<VirtualHost 192.168.56.98:80>
DocumentRoot /var/www/html/site3
ServerName site3
</VirtualHost>

<VirtualHost 192.168.56.98:80>
DocumentRoot /var/www/html/site4
ServerName site4
</VirtualHost>

<VirtualHost 192.168.56.98:80>
DocumentRoot /var/www/html/site5
ServerName site5
</VirtualHost>
```

On créé les répertoires et les fichiers index.html
```bash
[root@websrv ~]# mkdir /var/www/html/site4 /var/www/html/site5
[root@websrv ~]# sed 's/Titre 1/Site 4/' /var/www/html/index.html > /var/www/html/site4/index.html
[root@websrv ~]# sed 's/Titre 1/Site 5/' /var/www/html/index.html > /var/www/html/site5/index.html
[root@websrv ~]# systemctl restart httpd.service
```

Il faut maintenant faire en sorte de contacter le serveur Web avec les noms site3, 4 et 5 et non avec l'adresse IP 192.168.56.98

Sur un ordinateur avec __un systeme d'exploitation__ on édite /etc/hosts

```bash
alan@e4310:~$ sudo vi /etc/hosts
alan@e4310:~$ grep 56.98 /etc/hosts
192.168.56.98 site3
192.168.56.98 site4
192.168.56.98 site5
```

Sous windows il faut ajouter ces mêmes ligne dans le fichier c:\windows\system32\driver\etc\hosts

Puis on navigue sur les sites :

http://site3/
http://site4/
http://site5/

Si on utilise l'adresse IP http://192.168.56.98 on tombe sur le site3. Le premier NameVirtualHost de la configuration apache sur un socket est alors le site par défaut sur ce socket.
